TRAIN=newtrain.csv
MAIN=mbgm.SparkFeatureSort
WAIT=1
ENABLE_LOG=false
TARGET=target/scala-2.11/mbgm2017-assembly-1.0.jar
 
nohup $HOME/spark/bin/spark-submit --master spark://genomics-ecs1:7077 --deploy-mode client --driver-memory 55G --driver-cores 5 --executor-memory 20G  --executor-cores 2 --conf spark.default.parallelism=810 --conf spark.driver.maxResultSize=5g --conf spark.network.timeout=360000 --conf spark.eventLog.enabled=$ENABLE_LOG --class $MAIN $TARGET \
$TRAIN all_field_count.txt.reduced &
