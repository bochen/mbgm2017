package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkFeatureSort extends LazyLogging {


  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("FeatureReduce")
    val sc = new SparkContext(conf)
    main(args, sc)
  }


  def main(args: Array[String], sc: SparkContext): Unit = {

    val fin = args(1)

    val (trainX, trainy) = Utils.read_train_csv(args(0))
    val feats = sc.parallelize(Utils.read_text(fin), 1080).map(_.split("\t")).map(u => (Field(u(0)), u(1).toInt))
    val X = sc.broadcast(trainX.map(u => u.map(_ > 0)))
    val y = sc.broadcast(trainy)
    val filtered = feats.map {
      a =>
        val feat = a._1
        val featval = feat.value(X.value)
        val zipval = featval.zip(y.value)
        val one = zipval.filter(_._1 == 0).map(_._2)
        val two = zipval.filter(_._1 == 1).map(_._2)
        val score: Double = math.abs(Utils.mean(one) - Utils.mean(two)) / (Utils.stdev(one) + Utils.stdev(two))
        (feat, a._2, score)
    }.sortBy(u => (-u._3, u._1.size, -math.abs(u._2-2104)))
      .map(u => f"${u._1.toString}\t${u._2}\t${u._3}%.5f")
      .collect

    Utils.write_text(filtered, fin + ".sorted")
    println("end")
    X.destroy()
    y.destroy()
    sc.stop()

  }


}