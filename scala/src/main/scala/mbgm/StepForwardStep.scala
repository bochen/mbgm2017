package mbgm

import com.typesafe.scalalogging.LazyLogging

/**
  * Created by Lizhen Shi on 6/8/17.
  */
@SerialVersionUID(1234)
abstract class StepForwardStep(val candidates: Array[Field], val inital_fields: Array[Field], inital_score: Score) extends Serializable with LazyLogging {

  private var score: Score = inital_score

  @transient protected var data: Data = null

  def this(candidates: Array[Field]) = this(candidates, Array.empty[Field], inital_score = null)


  protected def run_configurations(configs: Array[Array[Field]]):Array[(Array[Field], Score)]

  protected def make_score(fields: Array[Field], data: Data): Score = {
    val X = fields.map(u => data(u))
    val y = data.y
    LinearLib.crossValid(X, y)
  }


  def run(data: Data) = {
    this.data = data

    val configs =
      Array(
        (if (score == null && inital_fields.nonEmpty) {
          Array(inital_fields)
        } else
          Array.empty[Field])
      ).asInstanceOf[Array[Array[Field]]] ++ candidates.map(x => inital_fields ++ Array(x))

    val this_scores = run_configurations(configs)

    val selected = this_scores.sortBy(x => -x._2.value).apply(0)

    (inital_fields ++ Array(selected._1), selected._2)
  }
}

@SerialVersionUID(1234)
class LocalStepForwardStep(candidates: Array[Field], inital_fields: Array[Field], inital_score: Score)
  extends StepForwardStep(candidates, inital_fields, inital_score) {
  override protected def run_configurations(configs: Array[Array[Field]]): Array[(Array[Field], Score)] = {
    configs.map {
      x =>
        (x, make_score(x, data))
    }
  }
}
