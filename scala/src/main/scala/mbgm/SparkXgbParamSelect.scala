package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}
import sext._
import smile.validation.CrossValidation

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkXgbParamSelect extends LazyLogging {

  case class Config(fname: String = "", feature_file: String = "", k: Int = 5, k_round: Int = 10, sleep: Int = 0)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("SparkXgbParamSelect") {

      opt[String]('i', "input").required().valueName("<file>").action((x, c) =>
        c.copy(fname = x)).text("input train data file")
      opt[String]('f', "features").required().valueName("<file>").action((x, c) =>
        c.copy(feature_file = x)).text("feature file")

      opt[Int]('k', "k").action((x, c) =>
        c.copy(k = x))
        .text("number of validation folders")


      opt[Int]("k_round").action((x, c) =>
        c.copy(k_round = x))
        .text("round of k-cross validation")


      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

    }

    parser.parse(args, Config())
  }

  def slice(X: Array[Array[Double]], idx: Array[Int]): Array[Array[Double]] = {
    idx.map(X(_))
  }

  def slice(y: Array[Double], idx: Array[Int]) = idx.map(y(_))


  implicit class Crossable[X](xs: Traversable[X]) {
    def cross[Y](ys: Traversable[Y]) = for {x <- xs; y <- ys} yield (x, y)
  }

  def run_single(data: Data, tuple: (Int, Int, Array[Int], Array[Int]), param: XgbParam, fields: Array[Field], config: Config) = {

    val strfields = fields.map(_.toString).mkString(",")

    val (round, k, trainidx, testidx) = tuple
    logger.info(s"start (fold $round/$k, [$param] ")

    val X = data(fields)
    val y = data.y
    require(X.length == y.length)

    val trainx = slice(X, trainidx)
    val trainy = slice(y, trainidx)

    val testx = slice(X, testidx)
    val testy = slice(y, testidx)

    val model = new XGBoostModel()
    val paramMap: Map[String, Any] = List(
      "eta" -> param.eta,
      "colsample_bytree" -> param.colsample_bytree,
      "lambda" -> param.lambda,
      "max_depth" -> param.max_depth,
      "min_child_weight" -> param.min_child_weight,
      "subsample" -> param.subsample,
      "eval_metric" -> "rmse",
      "early_stop" -> 10,
      "objective" -> "reg:linear").toMap

    val evalrsme = model.train(trainx, trainy, testx, testy, paramMap, round = 500)
    val evalr2=1-evalrsme*evalrsme/Utils.variance(testy)

    var r2 = model.RSquared(testx, testy)
    val r2m = model.RSquared(trainx, trainy)
    logger.info(f"finish. round/fold $round/$k, R^2=$evalr2%.5f ($r2%.5f/$r2m%.5f), [$param]")
    (param, evalr2.toDouble)

  }


  def run(X: Array[Array[Int]], y: Array[Double], candidates: Vector[Field],
          config: Config, sc: SparkContext): (XgbParam, Double) = {
    val data = sc.broadcast(new Data(X.map(u => u.map(_ > 0)), y))

    val local_splits = (0 until config.k_round).map {
      j =>
        val instance = new CrossValidation(X.length, config.k)
        (0 until config.k).map {
          i =>
            val trainidx = instance.train(i)
            val testidx = instance.test(i)
            require(trainidx.length + testidx.length == X.length)
            (j, i, trainidx, testidx)
        }
    }.flatten

    val params = XgbParam.generate_candidates()

    val local_configuraitons = local_splits.cross(params).toSeq
    logger.info(s"#configurations=${local_configuraitons.length}, #split=${local_splits.length}")

    val fields = candidates.toArray
    logger.info(s"create ${fields.length} fields")

    val all_results = sc.parallelize(local_configuraitons, 1080).map {
      case (split, xgbparam) =>

        run_single(data.value, split, xgbparam, fields, config)
    }.reduceByKey(_ + _).map(x => (x._1, x._2 / local_splits.length)).sortBy(u => -u._2).collect()
    println("All scores:")
    all_results.foreach(u => println(u._1 + "\t" + u._2))

    val best_field = all_results.head

    logger.info(f"Best param found: R^2=${best_field._2}%.5f,  param=${best_field._1} ")

    data.destroy()

    best_field
  }

  def main(args: Array[String], sc: SparkContext): Unit = {
    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")

        val fname = config.fname

        val (trainX, trainy) = Utils.read_train_csv(fname)
        require(trainX.length == 4208)
        require(trainX(0).length == 551)
        require(trainy.length == 4208)

        val candidates = Utils.read_text(config.feature_file).map(_.split("\t")).map(u => Field(u(0))).toVector
        val fout = s"${fname}_xgbparam"


        logger.info(s"Run with data=${fname}, #fields=${candidates.length}")

        val new_record = run(trainX, trainy, candidates, config, sc)

        Utils.WriteObjectFromFile(new_record, fout)
        logger.info(s"write fields to $fout")

        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("SparkXgbParamSelect")
    val sc = new SparkContext(conf)
    main(args, sc)
    sc.stop()
  }


}
