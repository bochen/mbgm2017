package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkGenlibsvmData extends LazyLogging {


  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("SparkGenlibsvmData")
    val sc = new SparkContext(conf)
    main(args, sc)
  }


  def main(args: Array[String], sc: SparkContext): Unit = {

    val fin = args(1)
    val datafile = args(0)
    var trainX: Array[Array[Boolean]] = null
    var trainy: Array[Double] = null
    if (datafile.contains("train")) {
      val (t, tt) = Utils.read_train_csv(datafile)
      trainX = t.map(u => u.map(_ > 0))
      trainy=tt
    } else {
      val (t, _) = Utils.read_csv(datafile, has_y = false)
      trainX = t.map(u => u.map(_ > 0))
      trainy = trainX.indices.map(u => 0.0).toArray
    }
    require(trainX!=null)
    require(trainy!=null)
    val feats = sc.parallelize(Utils.read_text(fin), 1080).map(_.split("\t")).map(u => Field(u(0))).zipWithIndex().map(u => (u._1, u._2 + 1))
    val lines = trainX.zip(trainy).map {
      case (x, y) =>
        val s = feats.map {
          case (field, idx) =>
            (idx, field.value(x))
        }.filter(_._2 > 0).map(u => s"${u._1}:${u._2}").collect().mkString(" ")
        s"$x $s"
    }


    Utils.write_text(lines, fin + ".libsvm")
    println("end")
    sc.stop()

  }


}