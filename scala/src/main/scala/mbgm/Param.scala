package mbgm

/**
  * Created by Lizhen Shi on 6/18/17.
  */
case class XgbParam(eta: Double, max_depth: Int, lambda: Double, subsample: Double, colsample_bytree: Double, min_child_weight: Double) {

  override def toString: String = {
    val k = "eta, max_depth,lambda, subsample, colsample_bytree, min_child_weight".split(",").map(_.trim)
    val v = List(eta, max_depth, lambda, subsample, colsample_bytree, min_child_weight)
    k.zip(v).map { case (a, b) => s"$a:$b" }.mkString(" ")
  }
}

object XgbParam {
  def generate_candidates(): Array[XgbParam] = {
    val params = Array(0.3, 0.1, 0.03, 0.01).map {
      eta =>
        Array(1, 0.3, 0.1).map { lambda =>
          Array(1, 0.8).map { subsample =>
            Array(1, 0.8, 0.5).map { colsample_bytree =>
              Array(1, 2, 3).map { min_child_weight =>
                Array(3, 5, 8, 10).map {
                  max_depth =>
                    new XgbParam(eta = eta, max_depth = max_depth, lambda = lambda,
                      subsample = subsample, colsample_bytree = colsample_bytree, min_child_weight = min_child_weight)
                }
              }.flatten
            }.flatten
          }.flatten
        }.flatten
    }.flatten
    params
  }
}
