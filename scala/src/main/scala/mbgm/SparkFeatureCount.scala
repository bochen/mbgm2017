package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}
import sext._

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkFeatureCount extends LazyLogging {

  case class Config(fname: String = "", n: Int = 1, min_count: Int = 10, sleep: Int = 0)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("FeatureCount") {

      opt[String]('i', "input").required().valueName("<file>").action((x, c) =>
        c.copy(fname = x)).text("input train data file")

      opt[Int]('n', "numcol").required().action((x, c) =>
        c.copy(n = x))
        .text("number of columns in a field")
      opt[Int]('m', "min_count").action((x, c) =>
        c.copy(min_count = x))
        .text("minimum count to preserve")

      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

    }

    parser.parse(args, Config())
  }


  def run(X: Array[Array[Int]], prev_fields: Array[Field], min_count: Int, sc: SparkContext): Array[(Field, Int)] = {
    val max_n = X(0).length
    val data = sc.broadcast(X.map(Record(_)))
    val expand_cols = {
      val tmp = X.map(u => u.map(u => if (u > 0) 1 else 0))
      (0 until X(0).length).map(j => (j, 0.until(X.length).map(i => tmp(i)(j)).sum)).filter(_._2 >= min_count).map(_._1).toArray
    }
    val fields = sc.parallelize(prev_fields).map(_.expand(expand_cols)).flatMap(x => x).distinct()
    val candidate_cnt = fields.count
    logger.info(s"candidate fields number=${candidate_cnt}")
    val r = fields.map {
      f =>
        (f, data.value.map(f.value(_)).sum)
    }.filter(_._2 >= min_count)
      .map {
        a =>
          (a._1.value(data.value).map(u => u > 0).toVector, a)
      }
      //.groupByKey().map(u => u._2.head)
        .reduceByKey((u,v)=>u ).map(_._2)
      .collect
    data.destroy()
    logger.info(s"filter ${r.length} fields out of ${candidate_cnt}")
    r
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("FeatureCount")
    val sc = new SparkContext(conf)
    main(args, sc)
  }

  def main(args: Array[String], sc: SparkContext): Unit = {
    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")

        val min_count = config.min_count
        val fname = config.fname
        val n = config.n
        val fout = s"field_count_$n.txt"

        val prev_fields =
          if (n == 1)
            Array(Field.empty())
          else {
            val fin = s"field_count_${n - 1}.txt"
            //Utils.ReadObjectFromFile[Array[(Field, Int)]](fin).filter(x=>x._2>=config.min_count).map(_._1)
            Utils.read_text(fin).map(_.split("\t")).filter(x=>x(1).toInt>=config.min_count).map(_(0)).map(Field(_))
          }

        logger.info(s"Run with data=${fname}, n=$n, prev_len=${prev_fields.length}")

        val (trainX, trainy) = Utils.read_csv(fname, has_y = true)
        require(trainX.length == 4209)
        require(trainX(0).length == 551)
        require(trainy.length == 4209)
        val field_counts = run(trainX, prev_fields, min_count, sc)
        //Utils.WriteObjectFromFile(field_counts, fout)
        Utils.write_text(field_counts.map(u => u._1.toString + "\t" + u._2.toString), fout)
        logger.info(s"write ${field_counts.length} fields to $fout")

        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
        sc.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }


  }
}
