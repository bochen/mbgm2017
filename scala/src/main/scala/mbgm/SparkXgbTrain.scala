package mbgm

import com.typesafe.scalalogging.LazyLogging
import ml.dmlc.xgboost4j.scala.spark.XGBoost
import ml.dmlc.xgboost4j.scala.{Booster, spark}
import org.apache.spark.SparkConf
import org.apache.spark.mllib.evaluation.RegressionMetrics
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import sext._

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkXgbTrain extends LazyLogging {


  case class Config(fname: String = "", eta: Double = 0.3, max_depth: Int = 6, lambda: Double = 1,
                    subsample: Double = 1, colsample_bytree: Double = 1, min_child_weight: Double = 1,
                    n_fold: Int = 5, n_worker: Int = 2, n_round: Int = 100)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("FeatureCount") {

      opt[String]('i', "input").required().valueName("<file>").action((x, c) =>
        c.copy(fname = x)).text("input train data file")

      opt[Int]("n_fold").action((x, c) => c.copy(n_fold = x))
      opt[Int]("n_worker").action((x, c) => c.copy(n_worker = x))
      opt[Int]("n_round").action((x, c) => c.copy(n_round = x))


      opt[Int]("max_depth").action((x, c) => c.copy(max_depth = x))

      opt[Double]("eta").action((x, c) => c.copy(eta = x))
      opt[Double]("lambda").action((x, c) => c.copy(lambda = x))
      opt[Double]("subsample").action((x, c) => c.copy(subsample = x))
      opt[Double]("colsample_bytree").action((x, c) => c.copy(colsample_bytree = x))
      opt[Double]("min_child_weight").action((x, c) => c.copy(min_child_weight = x))

    }

    parser.parse(args, Config())
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("SparkXgbTrain")
    conf.registerKryoClasses(Array(classOf[Booster]))
    val sparkSession = SparkSession.builder().config(conf).getOrCreate()
    main(args, sparkSession)
    sparkSession.stop()
  }


  def split(dataDF: Array[Dataset[Row]]) = {
    val n = dataDF.length
    (0 until n).map {
      i =>
        val train = (0 until n).filter(_ != i).map(dataDF(_)).reduce { (u, v) => u.union(v) }
        (i, train, dataDF(i))
    }
  }

  def evaluateRegressionModel(model: spark.XGBoostModel,
                              data: DataFrame,
                              labelColName: String): (Double, Double) = {
    val fullPredictions = model.setExternalMemory(true).transform(data).cache()
    val predictions = fullPredictions.select("prediction")
      .rdd.map(_.getFloat(0))
      .map(_.toDouble)
    val labels = fullPredictions.select(labelColName).rdd
      .map(_.getDouble(0))
    val metrics = new RegressionMetrics(predictions.zip(labels))
    val RMSE = metrics.rootMeanSquaredError
    val r2 = metrics.r2
    fullPredictions.unpersist()
    (r2, RMSE)
  }

  def main(args: Array[String], sparkSession: SparkSession): Unit = {

    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")


        val dataDF = sparkSession.sqlContext.read.format("libsvm").load(config.fname)
          .randomSplit(0.until(config.n_fold).map(_ => 1.0 / config.n_fold).toArray) //.orderBy(rand())
        dataDF.foreach(_.cache())
        dataDF.foreach(_.show())
        val paramMap: Map[String, Any] = List(
          "eta" -> config.eta,
          "max_depth" -> config.max_depth,
          "colsample_bytree" -> config.colsample_bytree,
          "lambda" -> config.lambda,
          "min_child_weight" -> config.min_child_weight,
          "subsample" -> config.subsample,
          "eval_metric" -> "rmse",
          "objective" -> "reg:linear").toMap

        val results = split(dataDF).map {
          case (i, trainRDD, validRDD) =>
            val model = XGBoost.trainWithDataFrame(trainRDD, paramMap, config.n_round, nWorkers = config.n_worker, useExternalMemory = true)
            val (train_r2, train_rmse) = evaluateRegressionModel(model, trainRDD, "label")
            logger.info(f"Training R^2=$train_r2%.6f, RMSE=$train_rmse%.6f")
            val (test_r2, test_rmse) = evaluateRegressionModel(model, validRDD, "label")
            logger.info(f"Validation R^2=$test_r2%.6f, RMSE=$test_rmse%.6f")
            (train_r2, test_r2)
        }

        logger.info(f"Mean R^2: train=${results.map(_._1).sum / results.length}%.6f, test=${results.map(_._2).sum / results.length}%.6f")

        println("end")

        dataDF.foreach(_.unpersist())
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }


  }


}