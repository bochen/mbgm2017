package mbgm

/**
  * Created by Lizhen Shi on 6/8/17.
  */
@SerialVersionUID(10)
case class Field(val colidx: Set[Int]) extends Serializable {

  def add(col: Int): Field = {
    if (colidx.contains(col))
      null
    else
      new Field(this.colidx ++ Set(col))
  }

  def size = colidx.size

  def value(v: Array[Record]): Array[Int] = {
    v.map(value(_))
  }


  def expand(expand_cols: Array[Int]): Array[Field] = {
    val set1 = expand_cols.toSet
    val set2 = set1.diff(colidx)
    set2.map {
      i =>
        val idx = colidx ++ Set(i)
        new Field(idx)
    }.toArray
  }

  def expand2(expand_cols: Array[Int]): Array[PlusField] = {
    val set1 = expand_cols.toSet
    val set2 = set1.diff(colidx)
    set2.map {
      i =>
        new PlusField(this, i)
    }.toArray
  }


  def value(record: Array[Boolean]): Int = {
    colidx.map(record(_)).map(x => if (x) 1 else 0).product
  }

  def value(record: Record): Int = {
    value(record.bitSet)
  }

  def value(X: Array[Array[Boolean]]): Array[Int] = X.map(value(_))


  def expand(max_n: Int): Array[Field] = {
    require(max_n > (if (colidx.nonEmpty) colidx.max else 0))
    val set1 = 0.until(max_n).toSet
    val set2 = set1.diff(colidx)
    set2.map {
      i =>
        val idx = colidx ++ Set(i)
        new Field(idx)
    }.toArray
  }

  override def toString: String = {
    colidx.toArray.sorted.mkString("*")
  }

  def is_empty = colidx.isEmpty
}

case class PlusField(val field: Field, val col: Int) extends Serializable {
  val all_cols = new Field(field.colidx ++ Set(col))

  override def hashCode(): Int = all_cols.hashCode()

  override def toString: String = all_cols.toString

  override def equals(o: scala.Any): Boolean = {
    if (o.isInstanceOf[PlusField]) {
      all_cols.equals(o.asInstanceOf[PlusField].all_cols)
    } else {
      false
    }
  }
}

object Field {
  def empty(): Field = new Field(Set.empty[Int])

  def apply(s: String): Field = {
    new Field(s.split("\\*").map(_.toInt).toSet)
  }
}
