package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}
import sext._

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkFeatureCount2 extends LazyLogging {

  case class Config(fname: String = "", n: Int = 1, min_count: Int = 10, sleep: Int = 0)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("FeatureCount") {

      opt[String]('i', "input").required().valueName("<file>").action((x, c) =>
        c.copy(fname = x)).text("input train data file")

      opt[Int]('n', "numcol").required().action((x, c) =>
        c.copy(n = x))
        .text("number of columns in a field")
      opt[Int]('m', "min_count").action((x, c) =>
        c.copy(min_count = x))
        .text("minimum count to preserve")

      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

    }

    parser.parse(args, Config())
  }


  def run(X: Array[Array[Int]], _prev_fields: Array[Field],  config: Config, sc: SparkContext): Array[(Field, Int)] = {
    val max_n = X(0).length
    val data = sc.broadcast(X.map(Record(_)))
    val single_field_values = sc.broadcast((0 until X(0).length).map(u => (u, Field(Set(u)))).map(u => (u._1, u._2.value(data.value))).toMap)
    val prev_fields = sc.broadcast(
      sc.parallelize(_prev_fields).map(u => (u, u.value(data.value).map(_ > 0))).collect().toMap)
    val expand_cols = {
      val tmp = X.map(u => u.map(u => if (u > 0) 1 else 0))
      (0 until X(0).length).map(j => (j, 0.until(X.length).map(i => tmp(i)(j)).sum)).filter(_._2 >= config.min_count).map(_._1).toArray
    }
    val fields = sc.parallelize(_prev_fields).map(_.expand2(expand_cols)).flatMap(x => x).distinct()
    val candidate_cnt = fields.count
    logger.info(s"candidate fields number=${candidate_cnt}")
    val unqiue_fields = fields.map {
      case pfield =>
        val f = pfield.field
        val col = pfield.col
        val newf = f.add(col)

        val value = prev_fields.value(f).zip(single_field_values.value(col))
          .map { case (u, v) => if (u) v else 0 }

        (newf, value.sum, value.map(_ > 0).toArray)
    }.filter(_._2 >= config.min_count)
      .map(u => (u._3.toVector, u))
      .reduceByKey((u, v) => u)
      .map(_._2)

    val r = unqiue_fields.map(u=>(u._1,u._2))
      .collect
    data.destroy()
    logger.info(s"filter ${r.length} fields out of ${candidate_cnt}")
    r
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("FeatureCount")
    val sc = new SparkContext(conf)
    main(args, sc)
  }

  def main(args: Array[String], sc: SparkContext): Unit = {
    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")

        val min_count = config.min_count
        val fname = config.fname
        val n = config.n
        val fout = s"field_count_$n.txt"

        val prev_fields =
          if (n == 1)
            Array(Field.empty())
          else {
            val fin = s"field_count_${n - 1}.txt"
            //Utils.ReadObjectFromFile[Array[(Field, Int)]](fin).filter(x=>x._2>=config.min_count).map(_._1)
            Utils.wait_on_file_done(fin)
            println("reading file "+fin)
            Utils.read_text(fin).map(_.split("\t")).filter(x=>x(1).toInt>=config.min_count).map(_(0)).map(Field(_))
          }

        logger.info(s"Run with data=${fname}, n=$n, prev_len=${prev_fields.length}")

        val (trainX, trainy) = Utils.read_csv(fname, has_y = true)
        require(trainX.length == 4209)
        require(trainX(0).length == 567)
        require(trainy.length == 4209)
        val field_counts = run(trainX, prev_fields, config, sc)
        //Utils.WriteObjectFromFile(field_counts, fout)
        Utils.write_text_with_done(field_counts.map(u => u._1.toString + "\t" + u._2.toString), fout)
        logger.info(s"write ${field_counts.length} fields to $fout")

        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
        sc.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }


  }
}
