package mbgm

/**
  * Created by Lizhen Shi on 6/11/17.
  */
@SerialVersionUID(1)
case class Config(round:Int, fold:Int, lambda:Double) extends Serializable{

}
