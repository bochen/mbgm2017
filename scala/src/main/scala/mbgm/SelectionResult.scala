package mbgm

/**
  * Created by Lizhen Shi on 6/12/17.
  */
case class SelectionResult(fields: Array[Field], score: Double, info: Map[String, Double]) {

  def num_fields = fields.length

  override def toString: String = {
    val strf = fields.map(_.toString).mkString(",")
    f"$num_fields\t$score%.6f\t$strf"
  }
}

object SelectionResult {
  def empty: SelectionResult = {
    new SelectionResult(Array(Field.empty()), -1, Map.empty[String, Double])
  }
}
