package mbgm

import ml.dmlc.xgboost4j.scala.{Booster, DMatrix, XGBoost}
import smile.regression.RidgeRegression

/**
  * Created by Lizhen Shi on 6/18/17.
  */
abstract class Model {
  def RSquared(X: Array[Array[Double]], y: Array[Double]): Double = {
    val yhat = predict(X)
    val sse = (0 until X.length).map {
      j =>
        yhat(j) - y(j)
    }.map(x => x * x).sum
    val ymean = y.sum / y.length
    val tss = (0 until y.length).map {
      j =>
        y(j) - ymean
    }.map(x => x * x).sum
    1 - sse / tss
  }


  def predict(X: Array[Array[Double]]): Array[Double]

  def train(X: Array[Array[Double]], y: Array[Double], evalX: Array[Array[Double]], evaly: Array[Double]): Unit
}

object Model {
  def apply(name: String): Model = {
    if (name.equalsIgnoreCase("OLS")) {
      new OLSModel()
    } else if (name.equalsIgnoreCase("xgboost")) {
      new XGBoostModel()
    } else if (name.equalsIgnoreCase("ridge")) {
      new RidgeModel()
    } else {
      throw new Exception("unknown model " + name)
    }
  }
}

class RidgeModel(val lambda:Double =0.3) extends Model {
  var model: RidgeRegression = null

  def train(X: Array[Array[Double]], y: Array[Double]) = {
    model = new RidgeRegression(X, y, lambda)
  }


  override def predict(X: Array[Array[Double]]): Array[Double] = X.map(model.predict(_))

  override def train(X: Array[Array[Double]], y: Array[Double], evalX: Array[Array[Double]], evaly: Array[Double]): Unit = train(X, y)
}

class OLSModel extends Model {
  var model: OLS = null

  def train(X: Array[Array[Double]], y: Array[Double]) = {
    model = new OLS(X, y, true)
  }


  override def predict(X: Array[Array[Double]]): Array[Double] = X.map(model.predict(_))

  override def train(X: Array[Array[Double]], y: Array[Double], evalX: Array[Array[Double]], evaly: Array[Double]): Unit = train(X, y)
}

class XGBoostModel extends Model {

  override def predict(X: Array[Array[Double]]): Array[Double] = {
    val trainData =
      new DMatrix(X.flatten.map(_.toFloat), nrow = X.length, ncol = X(0).length, missing = 0)
    model.predict(trainData).map(u => u(0).toDouble)
  }

  val EARLY_STOP= 10

  val paramMap: Map[String, Any] = List(
    "eta" -> 0.3,
    "max_depth" -> 3,
    "subsample" -> 1,
    "colsample_bytree" -> 1,
    "min_child_weight" -> 1,
    "eval_metric" -> "rmse",
    "early_stop" -> EARLY_STOP,
    "objective" -> "reg:linear").toMap

  var model: Booster = null

  def make_DMatrix(X: Array[Array[Double]], y: Array[Double]) = {
    val trainData =
      new DMatrix(X.flatten.map(_.toFloat), nrow = X.length, ncol = X(0).length, missing = 0)
    trainData.setLabel(y.map(_.toFloat))
    trainData
  }

  override def train(X: Array[Array[Double]], y: Array[Double], evalX: Array[Array[Double]], evaly: Array[Double]): Unit = {
    train(X, y, evalX, evaly, paramMap, 100)
  }

  def train(X: Array[Array[Double]], y: Array[Double], evalX: Array[Array[Double]],
            evaly: Array[Double], paramMap: Map[String, Any], round: Int): Float = {
    val trainData = make_DMatrix(X, y)
    val evalData = make_DMatrix(evalX, evaly)

    // train the model
    val watches = Map(("eval", evalData))
    val metrics = Array.fill(watches.size, round)(-1.0f)
    model = XGBoost.train(trainData, paramMap, round, watches = watches,metrics=metrics,obj=null,eval=null)
    metrics(0).reverse.filter(_>0).take(EARLY_STOP).reverse.head
    //model = XGBoost.train(trainData, paramMap, round)

  }
}
