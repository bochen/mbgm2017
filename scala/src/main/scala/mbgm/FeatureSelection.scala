package mbgm


import breeze.linalg._
import breeze.numerics._
import breeze.stats.distributions._
import com.typesafe.scalalogging.LazyLogging


/**
  * Created by Lizhen Shi on 6/12/17.
  */
@SerialVersionUID(111)
class FeatureSelection(val th: Double = 0.025) extends LazyLogging with Serializable {

  @transient var _norm: Gaussian = null

  def norm = {
    if (_norm == null) _norm = Gaussian(0, 1)
    _norm
  }

  val cols = collection.mutable.ListBuffer.empty[Field]

  def zero_mean(X: Array[Array[Double]]): Array[Array[Double]] = {
    val m = X.length
    val n = X(0).length
    val mean = (0 until n).map {
      j =>
        X.indices.map(X(_)(j)).sum / m
    }
    X.indices.map {
      i =>
        (0 until n).map(j => X(i)(j) - mean(j)).toArray
    }.toArray
  }


  def zero_mean(x: Array[Double]) = {
    val mean = x.sum / x.length
    x.map(_ - mean)
  }

  var prev_r2 = 0.000000
  var curr_r2 = 0.0
  var r: DenseVector[Double] = null
  var curr_idx = -1
  var shrink_ratio: Double = 1

  var w = 0.5
  var f = 0
  val dw = 0.05


  def get_x(_X: Array[Array[Boolean]], u: Field) = {
    val a = u.value(_X).map(_.toDouble)
    val m = Utils.mean(a)
    a.map(_ - m)
  }

  def sel_features(_X: Array[Array[Boolean]], _y: Array[Double], fields: Array[Field]): Array[Field] = {
    logger.info(s"Use threshold=$th")
    val m = _X(0).length
    val n = _X.length
    val y = DenseVector(zero_mean(_y))


    val sst = y.dot(y) / n

    if (r == null) r = y


    var X: DenseMatrix[Double] = null //DenseMatrix((cols.map(u => get_x(_X, u))): _*).t

    (curr_idx + 1 until fields.length).foreach {
      idx =>
        val field = fields(idx)
        val x: DenseVector[Double] = DenseVector(get_x(_X, field))
        val t = test_features(X, x, r, n = n )
        val a: Double = norm.cdf(abs(t))
        val alpha = w / (1 + 1 + curr_idx - f)
        //        if ( a > 1 - alpha/2) {
        if (a > 1 - th) {
          cols.append(field)
          prev_r2 = curr_r2
          r = r - (shrink_ratio * (x dot r) / (1e-10 + x.dot(x))) * x
          curr_r2 = 1 - r.dot(r) / n / sst
          logger.info(f"$idx: Add $field to list[${cols.length}] with t=$a%5f, r2: $prev_r2%.5f => $curr_r2%.5f")
          X = DenseMatrix((cols.map(u => get_x(_X, u))): _*).t
          w += dw
          f = curr_idx + 1

        } else {
          w = w - alpha / (1 - alpha)
        }
        if (idx % 10000 == 0) {
          //          if (idx % 100 == 0) {
          logger.info(s"iteration $idx")
          Utils.WriteObjectFromFile(this, "featsel_snapshot.bin")
        }
        curr_idx = idx
    }
    cols.toArray
  }


  private def g(X: DenseMatrix[Double], _x: DenseVector[Double]): Double = {
    val x: DenseMatrix[Double] = DenseMatrix(_x.data).t
    val r = x.t * X * pinv(X.t * X) * X.t * x
    r(0, 0) / (_x.dot(_x) + 1e-10)
  }

  def test_features(X: DenseMatrix[Double], x: DenseVector[Double], _r: DenseVector[Double], n: Int ): Double = {

    //val r = _r - mean(_r)

    def f(a: DenseVector[Double]): Double = sqrt(a.dot(a))

    val m = if (X==null) 0 else X.cols
    val sigma = f(r) / math.sqrt(n.toDouble - m - 1)

    val gamma: Double = (r dot x) / f(x)

    if (false) {
      val r2: Double = if (X.cols != 0) g(X, x) else 0

      if (false && (r2 > 0.99 || gamma < 1e-6)) {
        0.0
      } else {

        gamma / (sigma * sqrt(1 - r2) + 1e-10)
      }
    } else {
      gamma / (sigma + 1e-12)

    }
  }
}

object FeatureSelection extends LazyLogging {

  def main(args: Array[String]): Unit = {
    var datafile: String = null
    var featfile: String = null
    var obj: FeatureSelection =
      if (args.length == 3 && !args(0).equals("resume")) {
        datafile = args(0)
        featfile = args(1)
        val th = args(2).toDouble
        new FeatureSelection(th)
      } else if (args.length == 4 && args(0).equals("resume")) {
        datafile = args(2)
        featfile = args(3)
        Utils.ReadObjectFromFile[FeatureSelection](args(1))
      } else {
        throw new Exception("cmd train_file feat_file or cmd resume snapshot train_file feat_file")
      }

    val (train, y) = Utils.read_train_csv(datafile)
    require(train.length == 4208)
    require(y.length == train.length)
    require(train(0).length == 551)

    val fields = Utils.read_text(featfile).map(_.split("\t")).map(_ (0)).map(Field(_))
    val selected_fields = obj.sel_features(train.map(x => x.map(_ > 0)), y, fields)
    Utils.write_text(selected_fields.map(u => u.toString), "feat_selected.txt")
  }


}
