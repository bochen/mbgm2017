package mbgm

/**
  * Created by Lizhen Shi on 6/8/17.
  */
@SerialVersionUID(1)
class Score extends Serializable{

  var _value:Double = Double.MinValue
  val params = scala.collection.mutable.HashMap.empty[String,Double]
  //This lower the better
  def value:Double = _value

  def setValue(v:Double) = _value=v

  override def toString: String = {
    val sp = params.map(x=>s"${x._1} -> ${x._2}").mkString(",")
    s"value: $value, params: $sp"
  }

}
