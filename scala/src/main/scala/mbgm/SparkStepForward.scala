package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}
import sext._
import smile.validation.CrossValidation

/**
  * Created by Lizhen Shi on 6/11/17.
  */



object SparkStepForward extends LazyLogging {

  case class Config(fname: String = "", feature_file: String = "", n: Int = 1, k: Int = 10, k_round: Int = 10, sleep: Int = 0)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("SparkStepForward") {

      opt[String]('i', "input").required().valueName("<file>").action((x, c) =>
        c.copy(fname = x)).text("input train data file")
      opt[String]('f', "features").required().valueName("<file>").action((x, c) =>
        c.copy(feature_file = x)).text("feature file")

      opt[Int]('n', "num_feature").required().action((x, c) =>
        c.copy(n = x))
        .text("number of features")

      opt[Int]('k', "k").action((x, c) =>
        c.copy(k = x))
        .text("number of validation folders")


      opt[Int]("k_round").action((x, c) =>
        c.copy(k_round = x))
        .text("round of k-cross validation")


      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

    }

    parser.parse(args, Config())
  }

  def slice(X: Array[Array[Double]], idx: Array[Int]): Array[Array[Double]] = {
    idx.map(X(_))
  }

  def slice(y: Array[Double], idx: Array[Int]) = idx.map(y(_))

  def run_single(data: Data, tuple: (Int, Int, Array[Int], Array[Int]), fields: Array[Field]) = {

    val strfields = fields.map(_.toString).mkString(",")

    val (round, k, trainidx, testidx) = tuple
    logger.info(s"start (fold $round/$k [$strfields]) ${fields.length}")

    val X = data(fields)
    val y = data.y
    require(X.length == y.length)

    val trainx = slice(X, trainidx)
    val triany = slice(y, trainidx)
    val model = new OLS(X, y)


    val testx = slice(X, testidx)
    val testy = slice(y, testidx)
    val testyhat = testx.map(model.predict(_))
    val sse = (0 until testx.length).map {
      j =>
        testyhat(j) - testy(j)
    }.map(x => x * x).sum
    val ymean = testy.sum / testy.length
    val tss = (0 until testy.length).map {
      j =>
        testy(j) - ymean
    }.map(x => x * x).sum

    val r2 = 1 - sse / tss
    val r2m = model.RSquared()
     logger.info(s"finish (fold $round/$k [$strfields]): R=$r2/$r2m")
    (fields.toVector, r2)

  }


  def run(X: Array[Array[Int]], y: Array[Double], candidates: Vector[Field], prev_record: SelectionResult,
          config: Config, sc: SparkContext): SelectionResult = {
    val data = sc.broadcast(new Data(X.map(u => u.map(_ > 0)), y))

    val local_configuraitons = (0 until config.k_round).map {
      j =>
        val instance = new CrossValidation(X.length, config.k)
        (0 until config.k).map {
          i =>
            val trainidx = instance.train(i)
            val testidx = instance.test(i)
            require(trainidx.length + testidx.length == X.length)
            (j, i, trainidx, testidx)
        }
    }.flatten

    val configuraitons = sc.broadcast(local_configuraitons)

    val curr_featers = prev_record.fields
    val fields = candidates.filter(!curr_featers.toSet.contains(_)).map(u => Array(u) ++ curr_featers).map(x => x.filter(u => !u.is_empty))
    logger.info(s"create ${fields.length} ${curr_featers.length + 1}-fields")
    val best_field = sc.parallelize(fields).cartesian(sc.parallelize(0.until(local_configuraitons.length))).map {
      u =>
        val idx = u._2
        run_single(data.value, configuraitons.value.apply(idx), u._1)
    }.reduceByKey(_ + _).map(x => (x._1, x._2 / local_configuraitons.length)).sortBy(u => -u._2).take(1).head


    configuraitons.destroy()
    data.destroy()

    new SelectionResult(best_field._1.toArray, best_field._2, Map.empty[String, Double])
  }

  def main(args: Array[String], sc: SparkContext): Unit = {
    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")

        val fname = config.fname
        val n = config.n
        val fout = s"selection_$n"

        val prev_records =
          if (n == 1)
            Array(SelectionResult.empty)
          else {
            val fin = s"selection_${n - 1}"
            Utils.ReadObjectFromFile[Array[SelectionResult]](fin)
          }

        val _prev_record = if (n == 1) prev_records else prev_records.filter(_.num_fields == n - 1)
        require(_prev_record.length == 1)
        val prev_record = _prev_record.head
        logger.info(s"Run with data=${fname}, n=$n, prev_score=${prev_record.score}")

        val (trainX, trainy) = Utils.read_csv(fname, has_y = true)
        require(trainX.length == 4209)
        require(trainX(0).length == 551)
        require(trainy.length == 4209)

        val candidates = Utils.ReadObjectFromFile[Array[(Field, Int)]](config.feature_file).map(_._1).toVector

        val new_record = run(trainX, trainy, candidates, prev_record, config, sc)

        val all_record = Array(new_record) ++ prev_records
        Utils.WriteObjectFromFile(all_record, fout)
        logger.info(s"write ${all_record.length} fields to $fout")

        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
        sc.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("SparkStepForward")
    val sc = new SparkContext(conf)
    main(args, sc)
  }
}
