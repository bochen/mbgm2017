package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}
import sext._
import smile.validation.CrossValidation

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkStepBackward extends LazyLogging {

  case class Config(fname: String = "", feature_file: String = "", model: String = "OLS",
                    n: Int = 1, k: Int = 5, k_round: Int = 10, sleep: Int = 0)

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("SparkStepBackward") {

      opt[String]('i', "input").required().valueName("<file>").action((x, c) =>
        c.copy(fname = x)).text("input train data file")
      opt[String]('f', "features").required().valueName("<file>").action((x, c) =>
        c.copy(feature_file = x)).text("feature file")

      opt[String]('m', "model").required().valueName("model").action((x, c) =>
        c.copy(model = x)).text("model to use")


      opt[Int]('n', "num_feature").required().action((x, c) =>
        c.copy(n = x))
        .text("number of features")

      opt[Int]('k', "k").action((x, c) =>
        c.copy(k = x))
        .text("number of validation folders")


      opt[Int]("k_round").action((x, c) =>
        c.copy(k_round = x))
        .text("round of k-cross validation")


      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

    }

    parser.parse(args, Config())
  }

  def slice(X: Array[Array[Double]], idx: Array[Int]): Array[Array[Double]] = {
    idx.map(X(_))
  }

  def slice(y: Array[Double], idx: Array[Int]) = idx.map(y(_))


  def run_single(data: Data, tuple: (Int, Int, Array[Int], Array[Int]), fields: Array[Field], config: Config) = {

    val strfields = fields.map(_.toString).mkString(",")

    val (round, k, trainidx, testidx) = tuple
    logger.info(s"start (round/fold $round/$k, ${fields.length}) ")

    val X = data(fields)
    val y = data.y
    require(X.length == y.length)

    val trainx = slice(X, trainidx)
    val trainy = slice(y, trainidx)

    val testx = slice(X, testidx)
    val testy = slice(y, testidx)

    val model = Model(config.model)
    model.train(trainx, trainy, testx, testy)


    var r2 = model.RSquared(testx, testy)
    val r2m = model.RSquared(trainx, trainy)
    logger.info(f"finish. round/fold $round/$k, R^2=$r2%.5f/$r2m%.5f ")
    (fields.toVector, r2)

  }


  def run(X: Array[Array[Int]], y: Array[Double], prev_record: SelectionResult, candidates: Vector[Field],
          config: Config, sc: SparkContext): SelectionResult = {
    val data = sc.broadcast(new Data(X.map(u => u.map(_ > 0)), y))

    val local_configuraitons = (0 until config.k_round).map {
      j =>
        val instance = new CrossValidation(X.length, config.k)
        (0 until config.k).map {
          i =>
            val trainidx = instance.train(i)
            val testidx = instance.test(i)
            require(trainidx.length + testidx.length == X.length)
            (j, i, trainidx, testidx)
        }
    }.flatten
    logger.info(s"#configurations=${local_configuraitons.length}")

    val configuraitons = sc.broadcast(local_configuraitons)
    val fields = if (prev_record == null) {
      Array(candidates.toArray)
    } else {
      val curr_feats = prev_record.fields
      curr_feats.indices.map {
        u =>
          curr_feats.zipWithIndex.filter(_._2 != u).map(_._1)
      }.toArray
    }
    logger.info(s"create ${fields.length} fields")

    val all_records = sc.parallelize(fields).cartesian(sc.parallelize(0.until(local_configuraitons.length))).repartition(1080)
    logger.info(s"try to find best out of ${all_records.count()} fields")
    val all_results = all_records.map {
      u =>
        val idx = u._2
        run_single(data.value, configuraitons.value.apply(idx), u._1, config)
    }.reduceByKey(_ + _).map(x => (x._1, x._2 / local_configuraitons.length)).sortBy(u => -u._2).collect()
    require(all_results.length == fields.length)
    println("All scores:")
    all_results.foreach(u=>println(u._2))
    val best_field = all_results.head


    val strfields = best_field._1.map(_.toString).mkString(",")
    logger.info(f"Best field found: R^2=${best_field._2}%.5f, #fields=${best_field._1.length} ")


    configuraitons.destroy()
    data.destroy()

    new SelectionResult(best_field._1.toArray, best_field._2, Map.empty[String, Double])
  }

  def main(args: Array[String], sc: SparkContext): Unit = {
    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")

        val fname = config.fname
        val n = config.n
        val fout = s"${config.model}_backward_selection_$n"

        val (trainX, trainy) = Utils.read_train_csv(fname)
        require(trainX.length == 4208)
        require(trainX(0).length == 551)
        require(trainy.length == 4208)

        val candidates = Utils.read_text(config.feature_file).map(_.split("\t")).map(u => Field(u(0))).toVector

        require(n > 0 && n <= candidates.length)

        val prev_record = if (n == candidates.length) {
          null
        } else {
          val fin = s"${config.model}_backward_selection_${n + 1}"
          Utils.ReadObjectFromFile[SelectionResult](fin)

        }
        logger.info(s"Run with data=${fname}, n=$n, prev_score=${if (prev_record == null) null else prev_record.score}")

        val new_record = run(trainX, trainy, prev_record, candidates, config, sc)

        Utils.WriteObjectFromFile(new_record, fout)
        logger.info(s"write fields to $fout")

        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("SparkStepBackward")
    val sc = new SparkContext(conf)
    main(args, sc)
    sc.stop()
  }
}

