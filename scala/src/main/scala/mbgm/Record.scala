package mbgm

/**
  * Created by Lizhen Shi on 6/7/17.
  */
case class Record(bitSet: Array[Boolean]) {

}

object Record {
  def sum(a: Array[Boolean]) = a.map(x => if (x) 1 else 0).sum

  def toString(a: Array[Boolean]): String = a.map(x => if (x) 1 else 0).mkString("")

  def toString(x: Array[Int]): String = x.mkString("")

  def apply(array: Array[Int]) = {

    new Record(array.map(_ > 0))
  }

  def hasBits(bitSet: Array[Boolean], other: Array[Boolean]): Boolean = {
    require(bitSet.length == other.length)
    bitSet.indices.foreach {
      i =>
        if (other(i) && !bitSet(i)) return false
    }
    true
  }

  def diff(bitSet: Array[Boolean], other: Array[Boolean]): Array[Boolean] = {
    require(bitSet.length == other.length)
    bitSet.indices.map {
      i =>
        if (other(i)) false else bitSet(i)
    }.toArray
  }

  def and(bitSet: Array[Boolean], other: Array[Boolean]) = {
    require(bitSet.length == other.length)
    bitSet.indices.map {
      i =>
        other(i) && bitSet(i)
    }
  }

  def or(bitSet: Array[Boolean], other: Array[Boolean]) = {
    require(bitSet.length == other.length)
    bitSet.indices.map {
      i =>
        other(i) || bitSet(i)
    }
  }

  //n ones
  def generate_subset(bitSet: Array[Boolean], n: Int): Array[Array[Boolean]] = {
    val e = bitSet.map(_ => false)
    bitSet.indices.filter(bitSet(_)).combinations(n).map {
      idx =>
        val a = collection.mutable.MutableList[Boolean](e: _*)
        idx.foreach(x => a(x) = true)
        a.toArray
    }.toArray
  }

  def generate_subset(bitSet: Array[Boolean], other: Array[Boolean]): Array[Array[Boolean]] = {
    val diffset = diff(bitSet, other)
    val subsets = generate_subset(diffset, 1).map(x => or(x, other).toArray)
    subsets
  }
}
