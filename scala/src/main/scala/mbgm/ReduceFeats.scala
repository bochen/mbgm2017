package mbgm

/**
  * Created by Lizhen Shi on 6/12/17.
  */
object ReduceFeats {
  def main(args: Array[String]): Unit = {

    val (trainX, trainy) = Utils.read_csv(args(0), has_y = true)
    val feats=Utils.ReadObjectFromFile[Array[(Field, Int)]](args(1))
    val X= trainX.map(u=>u.map(_>0))
    val filtered= feats.map {
      a=>
        val feat=a._1
        (feat.value(X).map(u=>u>0).toVector,a)
    }.groupBy(u=>u._1)
      .map(u=>u._2.head._2)
      .map(u=>u._1.toString+"\t"+u._2.toString)
    filtered.foreach(println)

  }
}
