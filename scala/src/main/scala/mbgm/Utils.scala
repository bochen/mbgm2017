package mbgm

import java.io._
import java.nio.file.{Files, Paths, StandardCopyOption}

import scala.io.Source

/**
  * Created by Lizhen Shi on 6/7/17.
  */
object Utils {

  def write_text_with_temp(strings: Array[String], fout: String) = {
    val tmpname = s"${fout}_tmp"
    write_text(strings, tmpname)
    Files.move(Paths.get(tmpname), Paths.get(fout), StandardCopyOption.REPLACE_EXISTING)
  }

  def write_empty_file(str: String) = {
    write_text(Array(), str)
  }

  def write_text_with_done(strings: Array[String], fout: String) = {
    write_text_with_temp(strings, fout)
    write_empty_file(s"${fout}.done")
  }


  def wait_on_file_done(fin: String): Boolean = {
    wait_on_file(s"${fin}.done")
  }

  def wait_on_file(fin: String): Boolean = {
    val path = new File(fin)
    while (true) {
      if (path.exists()) return true
      Thread.sleep(10*1000)
    }
    false
  }

  def stdev(one: Array[Double]): Double =  {
    val m = mean(one)
    math.sqrt(one.map(_-m).map(u=>u*u).sum/one.length)
  }

  def variance(one: Array[Double]): Double =  {
    val m = mean(one)
     one.map(_-m).map(u=>u*u).sum/one.length
  }

  def mean(one: Array[Double]): Double = {
    one.sum/one.length
  }

  def write_text(strings: Array[String], fout: String) = {
    // FileWriter
    val file = new File(fout)
    val bw = new BufferedWriter(new FileWriter(file))
    strings.foreach {
      s =>
        bw.write(s)
        bw.write("\n")
    }
    bw.close()
  }

  def read_text(fname: String): Array[String] = {
    val src = Source.fromFile(fname)
    src.getLines().toArray
  }

  def read_csv(csv: String, has_y: Boolean = true): (Array[Array[Int]], Array[Double]) = {
    val src = Source.fromFile(csv)
    val iter = src.getLines().drop(1).map(_.split(",")).map(_.drop(1)).toArray
    val X = (if (has_y)
      iter.map(_.drop(1))
    else
      iter).map(x => x.map(_.toInt)).toArray
    val y = if (has_y) {
      iter.map(_.head.toDouble).toArray
    } else null

    (X, y)
  }

  def read_train_csv(csv: String): (Array[Array[Int]], Array[Double]) = {
    val src = Source.fromFile(csv)
    val iter = src.getLines().drop(1).map(_.split(",")).map(_.drop(1)).map {
      a =>
        (a(0).toDouble, a.drop(1).map(_.toInt))
    }
      .filter(_._1 < 200).toArray

    (iter.map(_._2), iter.map(_._1))
  }


  def home: String = System.getProperty("user.home")

  def ReadObjectFromFile[A](filename: String)(implicit m: scala.reflect.Manifest[A]): A = {
    val input = new ObjectInputStream(new FileInputStream(filename))
    val obj = input.readObject()
    val r = obj match {
      case x if m.erasure.isInstance(x) => x.asInstanceOf[A]
      case _ => sys.error("Type not what was expected when reading from file")
    }
    input.close()
    r
  }

  def WriteObjectFromFile[A](obj: A, filename: String)(implicit m: scala.reflect.Manifest[A]) = {
    val output = new ObjectOutputStream(new FileOutputStream(filename))
    output.writeObject(obj)
    output.close()

  }

}
