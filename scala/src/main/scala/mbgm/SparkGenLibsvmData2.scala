package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkGenLibsvmData2 extends LazyLogging {


  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("SparkGenlibsvmData")
    val sc = new SparkContext(conf)
    main(args, sc)
  }


  def main(args: Array[String], sc: SparkContext): Unit = {

    val fin = args(1)
    val datafile = args(0)
    var trainX: Array[Array[Boolean]] = null
    var trainy: Array[Double] = null
    if (datafile.contains("train")) {
      val (t, tt) = Utils.read_train_csv(datafile)
      trainX = t.map(u => u.map(_ > 0))
      trainy = tt
    } else {
      val (t, _) = Utils.read_csv(datafile, has_y = false)
      trainX = t.map(u => u.map(_ > 0))
      trainy = trainX.indices.map(u => 0.0).toArray
    }
    require(trainX != null)
    require(trainy != null)
    val data = sc.broadcast(trainX)
    val feats = sc.parallelize(Utils.read_text(fin), 1080).map(_.split("\t")).map(u => Field(u(0))).zipWithIndex().map(u => (u._1, u._2 + 1))
    val line_values = feats.map {
      case (field, idx) =>
        field.value(data.value).zipWithIndex.filter(_._1 > 0).map(u => (u._2, (idx, u._1)))

    }.flatMap(u => u).groupByKey().map{u =>
      (u._1, u._2.toArray.sortBy(_._1).map(v => s"${v._1}:${v._2}").mkString(" "))
    }.sortByKey().collect()

    val lines = line_values.map {
      case (i, s) =>
        val y = trainy(i)
        s"$y $s"
    }

    Utils.write_text(lines, fin + ".libsvm")
    println("end")
    data.destroy()
    sc.stop()

  }


}