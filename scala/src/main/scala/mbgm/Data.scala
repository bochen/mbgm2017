package mbgm

/**
  * Created by Lizhen Shi on 6/8/17.
  */
case class Data(  X:Array[Array[Boolean]],   y: Array[Double]) {

  require(X.length==y.length)

  def apply(field: Field): Array[Double] = {
    X.map(field.value(_).toDouble)
  }

  def apply(fields: Array[Field]): Array[Array[Double]] = {
    X.map{
      x=>
        fields.map(_.value(x).toDouble)
    }
  }


}
