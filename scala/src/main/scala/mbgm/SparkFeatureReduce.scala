package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkFeatureReduce extends LazyLogging {


  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("FeatureReduce")
    val sc = new SparkContext(conf)
    main(args, sc)
  }

  def main(args: Array[String], sc: SparkContext): Unit = {

    val fin = args(1)

    val (trainX, trainy) = Utils.read_csv(args(0), has_y = true)
    //val feats = sc.parallelize(Utils.ReadObjectFromFile[Array[(Field, Int)]](fin), 1080)
    val feats = sc.parallelize(Utils.read_text(fin).map(_.split("\t")).map(u => (Field(u(0)), u(1).toInt)))
    val X = sc.broadcast(trainX.map(u => u.map(_ > 0)))
    val filtered = feats.map {
      a =>
        val feat = a._1
        (feat.value(X.value).map(u => u > 0).toVector, a)
    }.reduceByKey((u, v) => u).map(_._2)
      .map(u => u._1.toString + "\t" + u._2.toString).collect

    Utils.write_text(filtered, fin + ".reduced")
    X.destroy()
    println("end")
    sc.stop()

  }


}