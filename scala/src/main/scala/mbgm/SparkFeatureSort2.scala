package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkFeatureSort2 extends LazyLogging {


  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("FeatureReduce")
    val sc = new SparkContext(conf)
    main(args, sc)
  }


  def main(args: Array[String], sc: SparkContext): Unit = {

    val fin = args(1)

    val (trainX, tmp_trainy) = Utils.read_train_csv(args(0))
    val y_mean = Utils.mean(tmp_trainy)
    val trainy = tmp_trainy.map(_ - y_mean)
    val feats = sc.parallelize(Utils.read_text(fin), 1080).map(_.split("\t")).map(u => (Field(u(0)), u(1).toInt))
    val X = sc.broadcast(trainX.map(u => u.map(_ > 0)))
    val y = sc.broadcast(trainy)
    val tss = sc.broadcast(tmp_trainy.map(u => u * u).sum)
    val filtered = feats.map {
      a =>
        val feat = a._1
        val featval = feat.value(X.value)
        val zipval = featval.zip(y.value)
        val score2 = {
          val two = zipval.filter(_._1 == 1).map(_._2)
          val two_mean = Utils.mean(two)
          (two.map(u => u * u).sum - two.map(_ - two_mean).map(u => u * u).sum) / tss.value
        }
        val score1 = if (true) {
          val two = zipval.filter(_._1 == 0).map(_._2)
          val two_mean = Utils.mean(two)
          (two.map(u => u * u).sum - two.map(_ - two_mean).map(u => u * u).sum) / tss.value
        } else 0.0

        (feat, a._2, (score1 + score2) * 100)
    }.sortBy(u => (-u._3, u._1.size, -math.abs(u._2 - 2104)))
      .map(u => f"${u._1.toString}\t${u._2}\t${u._3}%.5f")
      .collect

    Utils.write_text(filtered, fin + ".sorted2")
    println("end")
    tss.destroy()
    X.destroy()
    y.destroy()
    sc.stop()

  }


}