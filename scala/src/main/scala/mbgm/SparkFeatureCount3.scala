package mbgm

import com.typesafe.scalalogging.LazyLogging
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import sext._

import scala.util.Random

/**
  * Created by Lizhen Shi on 6/11/17.
  */


object SparkFeatureCount3 extends LazyLogging {

  case class Config(fname: String = "", n: Int = 1, n_sample: Int = 40000, min_count: Int = 10, sleep: Int = 0,
                    s: Int = 0, k: Int = 1000000, group_ratio: Double = 0.5, wait_input:Boolean=false )

  def parse_command_line(args: Array[String]): Option[Config] = {
    val parser = new scopt.OptionParser[Config]("FeatureCount") {

      opt[String]('i', "input").required().valueName("<file>").action((x, c) =>
        c.copy(fname = x)).text("input train data file")

      opt[Int]('n', "numcol").required().action((x, c) =>
        c.copy(n = x))
        .text("number of columns in a field")

      opt[Int]('k', "k").required().action((x, c) =>
        c.copy(k = x))
        .text("desired count")

      opt[Int]('s', "s").action((x, c) =>
        c.copy(s = x))
        .text("sample method")
      opt[Int]("n_sample").action((x, c) =>
        c.copy(n_sample = x))

      opt[Unit]("wait_input").action((x, c) =>
        c.copy(wait_input = true))

      opt[Double]('g', "gr").action((x, c) =>
        c.copy(group_ratio = x))
        .text("group_ratio")

      opt[Int]('m', "min_count").action((x, c) =>
        c.copy(min_count = x))
        .text("minimum count to preserve")

      opt[Int]("wait").action((x, c) =>
        c.copy(sleep = x))
        .text("wait $slep second before stop spark session. For debug purpose, default 0.")

    }

    parser.parse(args, Config())
  }


  def sample0(unqiue_fields: RDD[(Field, Int, Array[Boolean])], desired_count: Int, data: Broadcast[Array[Record]], config: Config): Array[(Field, Int)] = {
    unqiue_fields.map(u => (u._1, u._2)).takeSample(withReplacement = false, num = desired_count)
  }


  def sum(a: Array[Boolean]) = a.map(u => if (u) 1 else 0).sum.toDouble

  def reduce_group_fields(_group: Array[(Int, Field, Array[Boolean], Double)], desired_count: Long, config: Config) = {
    var group = _group
    val set = collection.mutable.ArrayBuffer.empty[(Int, Field, Array[Boolean], Double)]

    def is_sim(head: (Int, Field, Array[Boolean], Double)): Boolean = {
      val (_, f1, a1, v1) = head
      set.foreach {
        case (_, f2, a2, v2) =>
          val d = a1.zip(a2).map(u => u._1 != u._2).map(u => if (u) 1 else 0).sum
          if (d < 80) return true
          val s = a1.zip(a2).map(u => u._1 && u._2).map(u => if (u) 1 else 0).sum
          if (2 * s / (v1 + v2) > config.group_ratio) return true
      }
      false
    }

    var cnt = 0
    while (set.length < desired_count && group.nonEmpty) {
      val head = group.head
      if (!is_sim(head)) {
        set.append(head)
      }
      group = group.tail
      cnt += 1
    }
    (set.toArray, (cnt, _group.length))
  }

  def sample1(unqiue_fields: RDD[(Field, Int, Array[Boolean])], desired_count: Int, data: Broadcast[Array[Record]], config: Config): Array[(Field, Int)] = {
    val ratio = desired_count / unqiue_fields.count().toDouble
    val rdd1 = unqiue_fields.map(u => (u._2 / 50, u._1, u._3, u._2.toDouble)).groupBy(_._1).map {
      case (groupid, array) =>
        val n = math.max(1, array.size / 500)
        if (n == 1)
          Seq((array))
        else
          Random.shuffle(array).zipWithIndex.map(u => (u._1, u._2 % n)).groupBy(x => x._2).map(u => u._2.map(_._1))
    }.flatMap(u => u).repartition(3000)

    val rdd = rdd1.map {
      case a =>
        val group = Random.shuffle(a.toSeq).toArray
        val groupid = group.head._1
        val r = math.ceil(group.length * ratio).toInt
        logger.info(s"Processing group $groupid with ${group.length} records, desire $r records")
        reduce_group_fields(group, r, config)
    }.cache()

    val counts = rdd.map(_._2).collect()
    val group_ratio = counts.map(_._1).sum.toDouble / counts.map(_._2).sum
    logger.info(f"Group exploration ratio=${group_ratio}%5f")
    val results = rdd.map(_._1).flatMap(x => x).map(u => (u._2, u._4.toInt))

    rdd.unpersist()
    results.collect()
  }


  def run(X: Array[Array[Int]], _prev_fields: Array[Field], config: Config, sc: SparkContext): Array[(Field, Int)] = {
    val data = sc.broadcast(X.map(Record(_)))
    val single_field_values = sc.broadcast((0 until X(0).length).map(u => (u, Field(Set(u)))).map(u => (u._1, u._2.value(data.value))).toMap)
    val prev_fields = sc.broadcast(
      sc.parallelize(_prev_fields).map(u => (u, u.value(data.value).map(_ > 0))).collect().toMap)
    val expand_cols = {
      val tmp = X.map(u => u.map(u => if (u > 0) 1 else 0))
      (0 until X(0).length).map(j => (j, 0.until(X.length).map(i => tmp(i)(j)).sum)).filter(_._2 >= config.min_count).map(_._1).toArray
    }
    val fields = sc.parallelize(_prev_fields).map(_.expand2(expand_cols)).flatMap(x => x).distinct()
    val candidate_cnt = fields.count
    logger.info(s"candidate fields number=${candidate_cnt}")
    val unqiue_fields = fields.map {
      case pfield =>
        val f = pfield.field
        val col = pfield.col
        val newf = f.add(col)

        val value = prev_fields.value(f).zip(single_field_values.value(col))
          .map { case (u, v) => if (u) v else 0 }

        (newf, value.sum, value.map(_ > 0).toArray)
    }.filter(_._2 >= config.min_count)
      .map(u => (u._3.toVector, u))
      .reduceByKey((u, v) => u)
      .map(_._2).cache()

    logger.info(s"filter ${unqiue_fields.count} unique fields out of ${candidate_cnt}")

    val r = if (config.s == 0) sample0(unqiue_fields, config.k, data, config) else sample1(unqiue_fields, config.k, data, config)
    unqiue_fields.unpersist()
    prev_fields.destroy()
    single_field_values.destroy()
    data.destroy()
    logger.info(s"sample ${r.length} fields out of ${candidate_cnt}")
    r
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("FeatureCount")
    val sc = new SparkContext(conf)
    main(args, sc)
  }



  def main(args: Array[String], sc: SparkContext): Unit = {
    val options = parse_command_line(args)

    options match {
      case Some(_) =>
        val config = options.get

        logger.info(s"called with arguments\n${options.valueTreeString}")

        val fname = config.fname
        val n = config.n
        val fout = s"field_count_$n.txt"

        val _prev_fields =
          if (n == 1)
            Array(Field.empty())
          else {
            val fin = s"field_count_${n - 1}.txt"
            if(config.wait_input) Utils.wait_on_file_done(fin)
            Utils.read_text(fin).map(_.split("\t")).filter(x => x(1).toInt >= config.min_count).map(_ (0)).map(Field(_))
          }

        val prev_fields = Random.shuffle(_prev_fields.toSeq).take(config.n_sample).toArray

        logger.info(s"Run with data=${fname}, n=$n, prev_len=${prev_fields.length}")

        val (trainX, trainy) = Utils.read_csv(fname, has_y = true)
        require(trainX(0).length == 555)
        require(trainy.length == trainX.length)
        val field_counts = run(trainX, prev_fields, config, sc)
        Utils.write_text_with_done(field_counts.map(u => u._1.toString + "\t" + u._2.toString), fout)
        logger.info(s"write ${field_counts.length} fields to $fout")

        if (config.sleep > 0) Thread.sleep(config.sleep * 1000)
        sc.stop()
      case None =>
        println("bad arguments")
        sys.exit(-1)
    }


  }
}
