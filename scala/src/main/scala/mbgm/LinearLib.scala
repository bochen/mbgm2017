package mbgm

import com.typesafe.scalalogging.LazyLogging
import smile.regression.RidgeRegression
import smile.validation.CrossValidation


object TraversableWithStatistics {

  implicit class RichTraversable[+A](collection: Traversable[A]) {

    private def pow2[C, B >: C](x: C)(implicit num: Numeric[B]) = math.pow(num.toDouble(x), 2)

    def mean[B >: A](implicit num: Numeric[B]) = {
      num.toDouble(collection.sum[B]) / collection.size
    }

    def variance[B >: A](implicit num: Numeric[B]) = {
      collection.map(pow2[A, B]).mean - pow2(mean[B])
    }

  }

}

import mbgm.TraversableWithStatistics._


/**
  * Created by Lizhen Shi on 6/9/17.
  */
object LinearLib extends LazyLogging {


  def slice(X: Array[Array[Double]], idx: Array[Int]): Array[Array[Double]] = {
    idx.map(X(_))
  }

  def slice(y: Array[Double], idx: Array[Int]) = idx.map(y(_))

  def crossValid(X: Array[Array[Double]], y: Array[Double], k: Int = 4, C: Array[Double] = Array(1000, 300, 100, 30, 10, 3, 1, 0.3, 0.1)): Score = {
    val score = C.map {
      lambda =>
        val (mean, variance) = cross_valid(X, y, lambda, k)
        (lambda, mean)
    }.sortBy(x => -x._2).head
    val res = new Score
    res.setValue(score._2)
    res.params.put("lambda", score._1)
    logger.info(s"validation ($k folds, lambda=${C.mkString(",")}): $res")
    res
  }

  def cross_valid(X: Array[Array[Double]], y: Array[Double], lambda: Double, k: Int): (Double, Double) = {
    logger.info(s"start validation with $k folds and lambda $lambda")
    val n = X.length

    val rmses = 0.until(k).map {
      m =>
        val instance = new CrossValidation(n, 10)
        (0 until 10).map {
          i =>

            val trainidx = instance.train(i)
            val trainx = slice(X, trainidx)
            val triany = slice(y, trainidx)
            val model = new RidgeRegression(X, y, lambda)


            val testidx = instance.test(i)
            val testx = slice(X, testidx)
            val testy = slice(y, testidx)
            val testyhat = testx.map(model.predict(_))
            val sse = (0 until testx.length).map {
              j =>
                testyhat(j) - testy(j)
            }.map(x => x * x).sum
            val ymean = testy.sum / testy.length
            val ssy = (0 until testy.length).map {
              j =>
                testy(j) - ymean
            }.map(x => x * x).sum

            val r2 = 1 - sse / ssy
            val r = if (r2 > 0) math.sqrt(r2) else -math.sqrt(-r2)
            logger.info(s"validation (fold $m/$i/$k, lambda=$lambda): $r")
            r
        }
    }.flatten

    val res = (rmses.mean, math.sqrt(rmses.variance))
    logger.info(s"validation ($k folds, lambda=$lambda): mean=${res._1}, std=${res._2}")
    res
  }


}
