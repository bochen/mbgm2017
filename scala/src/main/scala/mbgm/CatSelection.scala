package mbgm

/**
  * Created by Lizhen Shi on 6/12/17.
  */
object CatSelection {
  def main(args: Array[String]): Unit = {

    args.foreach{
      fname=>
        val feats=Utils.ReadObjectFromFile[  SelectionResult ](fname)
            println(feats)
    }
  }
}
