package mbgm

import mbgm.SparkStepForward.main
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by Lizhen Shi on 6/12/17.
  */
object CatFeats {
  def main(args: Array[String]): Unit = {

    args.foreach{
      fname=>
        val feats=Utils.ReadObjectFromFile[Array[(Field, Int)]](fname)

        //println("field\tcnt")
        feats.foreach{
          x=>
            println(s"${x._1}\t${x._2}")
        }
    }
  }
}
