package mbgm

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class FeatureSelectionSpec extends FlatSpec with Matchers with BeforeAndAfter {

  "FeatureSelection" should "work on training data" in {
    val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"
    val (train, y) = Utils.read_train_csv(name)
    train.length should be(4208)
    y.length should be(train.length)
    train(0).length should be(551)

    val fields = Utils.read_text("field_count_1.txt.sorted2").map(_.split("\t")).map(_ (0)).map(Field(_))
    val selected_fields = new FeatureSelection(0.05 / 2).sel_features(train.map(x => x.map(_ > 0)), y, fields)
    println(s"Selected ${selected_fields.length} fields")
  }

  "FeatureSelection" should "resume" in {
    val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"
    FeatureSelection.main(Array("resume", "featsel_snapshot.bin", name, "field_count_1.txt.sorted"))
  }
}
