package mbgm

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */


/**
  * if (metrics != null)  { if (params.containsKey("early_stop"))  { val n_eary_stop: Int = params.get("early_stop").asInstanceOf[String].toInt
  * if (iter > n_eary_stop)  { val curr_score: Float = metrics(0)(iter)
  * val prev_score: Float = metrics(0)(iter - n_eary_stop)
  * if (curr_score > prev_score)  { Rabit.trackerPrint(String.format("[%d]Early stop with metric: %f\n", iter, curr_score))
  * break //todo: break is not supported
  * }
  * }
  * }
  * }
  */
class SparkXgbParamSelectSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"
  "SparkXgbParamSelect" should "work  on n=1" in {
    val args = s"-i $name   -f field_count_1.txt ".split(" ")
      .filter(_.nonEmpty)

    SparkXgbParamSelect.main(args, sc)
  }


}
