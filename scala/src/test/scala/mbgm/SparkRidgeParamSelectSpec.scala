package mbgm

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */


class SparkRidgeParamSelectSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"
  "SparkRidgeParamSelect" should "work  on n=1" in {
    val args = s"-i $name   -f field_count_1.txt ".split(" ")
      .filter(_.nonEmpty)

    SparkRidgeParamSelect.main(args, sc)
  }


}
