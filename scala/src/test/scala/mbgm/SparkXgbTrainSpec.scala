package mbgm

import com.holdenkarau.spark.testing.DataFrameSuiteBase
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class SparkXgbTrainSpec extends FlatSpec with Matchers with BeforeAndAfter with DataFrameSuiteBase {

  val name = Utils.home + "/mydev/mbgm2017/scala/field_count_1.txt.libsvm"
  //var name = Utils.home + "/spark/data/mllib/sample_linear_regression_data.txt"
  "SparkXgbTrain" should "work  " in {
    val args =  Array("-i", name)

    SparkXgbTrain.main(args, spark)
  }





}
