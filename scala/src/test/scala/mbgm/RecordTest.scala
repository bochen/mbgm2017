package mbgm

import org.junit.Test
import org.scalatest.{Assertion, Matchers}
import org.scalatest.junit.JUnitSuite

/**
  * Created by Lizhen Shi on 6/7/17.
  */
class RecordTest extends JUnitSuite with Matchers {


  @Test
  def generate_subset() = {
    val a = Array(true, false, true, true, false, true, true, false, false)
    Record.sum(a) should be(5)
    println(Record.toString(a))
    println

    val lst = Record.generate_subset(a, a.map(_ => false))
    lst.length should be(Record.sum(a))
    lst.foreach(x => x.length should be(a.length))
    lst.map(x => Record.toString(x)).foreach(println)
  }

  @Test
  def generate_subset2() = {
    val a = Array(true, false, true, true, false, true, true, false, false)
    val b = Array(true, false, false, false, false, false, false, false, false)

    println(Record.toString(a))
    println(Record.toString(b))
    println

    val lst = Record.generate_subset(a, b)
    lst.length should be(Record.sum(a) - 1)
    lst.foreach { x =>
      x.length should be(a.length)
      Record.hasBits(x, b) should be(true)
      Record.sum(x) should be(2)
      Record.and(x, b) should be(b)
    }
    lst.map(x => Record.toString(x)).foreach(println)
  }

  @Test
  def generate_subset3() = {
    val a = Array(true, false, true, true, false, true, true, false, false)
    val b = Array(true, false, false, false, false, false, true, false, false)

    println(Record.toString(a))
    println(Record.toString(b))
    println

    val lst = Record.generate_subset(a, b)
    lst.length should be(Record.sum(a) - 2)
    lst.foreach { x =>
      x.length should be(a.length)
      Record.hasBits(x, b) should be(true)
      Record.sum(x) should be(3)
      Record.and(x, b) should be(b)
    }
    lst.map(x => Record.toString(x)).foreach(println)
  }


  @Test
  def read_train_file(): Unit = {
    val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"
    val (train,y) = Utils.read_csv(name, has_y = true)
    train.length should be(4209)
    y.length should be (train.length)
    train(0).length should be(551)
    train.take(5).foreach(x => println(Record.toString(x)))
    y.take(5).foreach(println)
  }

  @Test
  def read_train_file2(): Unit = {
    val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"
    val (train,y) = Utils.read_train_csv(name)
    train.length should be(4208)
    y.length should be (train.length)
    train(0).length should be(551)
    train.take(5).foreach(x => println(Record.toString(x)))
    y.take(5).foreach(println)
  }

  @Test
  def read_test_file(): Unit = {
    val name = Utils.home + "/mydev/mbgm2017/input/newtest.csv"
    val (train,y) = Utils.read_csv(name, has_y = false)
    y should be(null)
    train.length should be(4209)
    train(0).length should be(551)
  }
}
