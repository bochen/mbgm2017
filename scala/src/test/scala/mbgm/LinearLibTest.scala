package mbgm

import org.junit.Test
import org.scalatest.Matchers
import org.scalatest.junit.JUnitSuite

/**
  * Created by Lizhen Shi on 6/10/17.
  */
class LinearLibTest extends JUnitSuite with Matchers {


  @Test
  def test_cross_validation(): Unit = {

    val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"

    val (trainX,trainy)= Utils.read_csv(name, has_y = true)
    trainX.length should be(4209)
    trainy.length should be(4209)

    val result = LinearLib.crossValid(trainX.map(x=>x.take(70).map(_.toDouble)),trainy)

  }


}
