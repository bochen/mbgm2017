package mbgm

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class SparkStepBackwardSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"
  "SparkStepBackward" should "work  on n=297" in {
    val args = s"-i $name -n 297  -f field_count_1.txt -k 2 --k_round 1 -m ols".split(" ")
      .filter(_.nonEmpty)

    SparkStepBackward.main(args, sc)
  }

  "SparkStepBackward" should "work  on ridge with n=297" in {
    val args = s"-i $name -n 297  -f field_count_1.txt -m ridge".split(" ")
      .filter(_.nonEmpty)

    SparkStepBackward.main(args, sc)
  }


  "SparkStepBackward" should "work  on xgboost with n=297" in {
    val args = s"-i $name -n 297  -f field_count_1.txt -m xgboost".split(" ")
      .filter(_.nonEmpty)

    SparkStepBackward.main(args, sc)
  }

  "SparkStepBackward" should "work  on n=296" in {
    val args = s"-i $name -n 296  -f field_count_1.txt -m ols".split(" ")
      .filter(_.nonEmpty)

    SparkStepBackward.main(args, sc)
  }
}
