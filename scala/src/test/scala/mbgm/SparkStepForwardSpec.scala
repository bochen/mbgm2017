package mbgm

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class SparkStepForwardSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"
  "SparkStepForward" should "work  on n=1" in {
    val args = s"-i $name -n 1  -f field_count_1".split(" ")
      .filter(_.nonEmpty)

    SparkStepForward.main(args, sc)
  }

  "SparkStepForward" should "work  on n=2" in {
    val args = s"-i $name -n 2 --min_count 10".split(" ")
      .filter(_.nonEmpty)

    SparkStepForward.main(args, sc)
  }

}
