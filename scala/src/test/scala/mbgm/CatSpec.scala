package mbgm

import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class CatSpec extends FlatSpec with Matchers with BeforeAndAfter {

   "CatFeats" should "work  on n=1" in {
    val args = "field_count_1".split(" ")
      .filter(_.nonEmpty)
    CatFeats.main(args)
  }

   "CatFeats" should "work  on n=2" in {
    val args = "field_count_2".split(" ")
      .filter(_.nonEmpty)
    CatFeats.main(args)
  }

  "ReduceFeats" should "work on n=1" in {
    val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"
    val args = Array(name, "field_count_1")
    ReduceFeats.main(args)
  }

}
