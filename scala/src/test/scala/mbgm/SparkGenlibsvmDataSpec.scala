package mbgm

import com.holdenkarau.spark.testing.SharedSparkContext
import org.scalatest.{BeforeAndAfter, FlatSpec, Matchers}

/**
  * Created by Lizhen Shi on 6/11/17.
  */
class SparkGenlibsvmDataSpec extends FlatSpec with Matchers with BeforeAndAfter with SharedSparkContext {

  val name = Utils.home + "/mydev/mbgm2017/input/newtrain.csv"
  "SparkFeatureCount" should "work  on n=1" in {
    val args = Array(name,"field_count_1.txt")

    SparkGenlibsvmData.main(args, sc)
  }

  "SparkFeatureCount" should "work  on n=2" in {
    val args = Array(name,"field_count_2.txt")

    SparkGenlibsvmData.main(args, sc)
  }



}
